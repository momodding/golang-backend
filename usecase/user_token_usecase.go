package usecase

import (
	"go-todos/domain/entity"
	"go-todos/domain/repository"
)

type UserTokenUsecase struct {
	tr repository.UserTokenRepository
}

var _ UserTokenReposInterface = &UserTokenUsecase{}

type UserTokenReposInterface interface {
	Save(*entity.UserToken) (*entity.UserToken, error)
	Update(*entity.UserToken) (*entity.UserToken, error)
	Delete(uint64) error
	GetById(uint64) (*entity.UserToken, error)
	GetByRefreshToken(string) (*entity.UserToken, error)
	GetAll() ([]entity.UserToken, error)
}

func (t *UserTokenUsecase) Save(user *entity.UserToken) (*entity.UserToken, error) {
	return t.tr.Save(user)
}

func (t *UserTokenUsecase) Update(user *entity.UserToken) (*entity.UserToken, error) {
	return t.tr.Update(user)
}

func (t *UserTokenUsecase) Delete(u uint64) error {
	return t.tr.Delete(u)
}

func (t *UserTokenUsecase) GetById(u uint64) (*entity.UserToken, error) {
	return t.tr.GetById(u)
}

func (t *UserTokenUsecase) GetByRefreshToken(s string) (*entity.UserToken, error) {
	return t.tr.GetByRefreshToken(s)
}

func (t *UserTokenUsecase) GetAll() ([]entity.UserToken, error) {
	return t.tr.GetAll()
}
