package usecase

import (
	"github.com/stretchr/testify/assert"
	"go-todos/domain/entity"
	"testing"
)

type fakeTodoRepo struct{}

var (
	saveTodoRepo    func(*entity.UserTodo) (*entity.UserTodo, error)
	updateTodoRepo  func(*entity.UserTodo) (*entity.UserTodo, error)
	deleteTodoRepo  func(uint64) error
	getByIdTodoRepo func(uint64) (*entity.UserTodo, error)
	getAllTodoRepo  func() ([]entity.UserTodo, error)
)

func (f *fakeTodoRepo) Save(todo *entity.UserTodo) (*entity.UserTodo, error) {
	return saveTodoRepo(todo)
}

func (f *fakeTodoRepo) Update(todo *entity.UserTodo) (*entity.UserTodo, error) {
	return updateTodoRepo(todo)
}

func (f *fakeTodoRepo) Delete(id uint64) error {
	return deleteTodoRepo(id)
}

func (f *fakeTodoRepo) GetById(id uint64) (*entity.UserTodo, error) {
	return getByIdTodoRepo(id)
}

func (f *fakeTodoRepo) GetAll() ([]entity.UserTodo, error) {
	return getAllTodoRepo()
}

var todoReposFake UserTodoReposInterface = &fakeTodoRepo{}

func TestSaveTodo_thenSuccess(test *testing.T) {
	saveTodoRepo = func(todos *entity.UserTodo) (*entity.UserTodo, error) {
		return &entity.UserTodo{
			ID:       1,
			TaskName: "lorem",
			TaskDesc: "ipsum",
		}, nil
	}

	todo := &entity.UserTodo{
		ID:       1,
		TaskName: "lorem",
		TaskDesc: "ipsum",
	}
	todoResult, err := todoReposFake.Save(todo)
	assert.Nil(test, err)
	assert.EqualValues(test, todoResult.ID, todo.ID)
	assert.EqualValues(test, todoResult.TaskName, todo.TaskName)
	assert.EqualValues(test, todoResult.TaskDesc, todo.TaskDesc)
}

func TestUpdateTodo_thenSuccess(test *testing.T) {
	updateTodoRepo = func(todos *entity.UserTodo) (*entity.UserTodo, error) {
		return &entity.UserTodo{
			ID:       1,
			TaskName: "lorem",
			TaskDesc: "ipsum",
		}, nil
	}

	todo := &entity.UserTodo{
		ID:       1,
		TaskName: "lorem",
		TaskDesc: "ipsum",
	}
	todoResult, err := todoReposFake.Update(todo)
	assert.Nil(test, err)
	assert.EqualValues(test, todoResult.ID, todo.ID)
	assert.EqualValues(test, todoResult.TaskName, todo.TaskName)
	assert.EqualValues(test, todoResult.TaskDesc, todo.TaskDesc)
}

func TestDeletedTodo_thenSuccess(test *testing.T) {
	deleteTodoRepo = func(u uint64) error {
		return nil
	}

	err := todoReposFake.Delete(1)
	assert.Nil(test, err)
}

func TestGetByIdTodo_thenSuccess(test *testing.T) {
	getByIdTodoRepo = func(u uint64) (*entity.UserTodo, error) {
		return &entity.UserTodo{
			ID:       1,
			TaskName: "lorem",
			TaskDesc: "ipsum",
		}, nil
	}

	todos, err := todoReposFake.GetById(1)
	assert.Nil(test, err)
	assert.EqualValues(test, todos.ID, 1)
	assert.EqualValues(test, todos.TaskName, "lorem")
	assert.EqualValues(test, todos.TaskDesc, "ipsum")
}

func TestGetAllTodo_thenSuccess(test *testing.T) {
	getAllTodoRepo = func() ([]entity.UserTodo, error) {
		return []entity.UserTodo{
			{
				ID:       1,
				TaskName: "lorem",
				TaskDesc: "ipsum",
			},
		}, nil
	}

	todos, err := todoReposFake.GetAll()
	assert.Nil(test, err)
	assert.EqualValues(test, len(todos), 1)
}
