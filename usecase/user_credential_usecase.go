package usecase

import (
	"go-todos/domain/entity"
	"go-todos/domain/repository"
)

type UserCredentialUsecase struct {
	tr repository.UserCredentialRepository
}

var _ UserReposInterface = &UserCredentialUsecase{}

type UserReposInterface interface {
	Save(*entity.User) (*entity.User, error)
	Update(*entity.User) (*entity.User, error)
	Delete(uint64) error
	GetById(uint64) (*entity.User, error)
	GetByEmailOrUsernameWhereActive(email string, username string) (*entity.User, error)
	GetAll() ([]entity.User, error)
}

func (t *UserCredentialUsecase) Save(user *entity.User) (*entity.User, error) {
	return t.tr.Save(user)
}

func (t *UserCredentialUsecase) Update(user *entity.User) (*entity.User, error) {
	return t.tr.Update(user)
}

func (t *UserCredentialUsecase) Delete(u uint64) error {
	return t.tr.Delete(u)
}

func (t *UserCredentialUsecase) GetById(u uint64) (*entity.User, error) {
	return t.tr.GetById(u)
}

func (t *UserCredentialUsecase) GetByEmailOrUsernameWhereActive(email string, username string) (*entity.User, error) {
	return t.tr.GetByEmailOrUsernameWhereActive(email, username)
}

func (t *UserCredentialUsecase) GetAll() ([]entity.User, error) {
	return t.tr.GetAll()
}
