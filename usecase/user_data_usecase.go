package usecase

import (
	"go-todos/domain/entity"
	"go-todos/domain/repository"
)

type UserDataUsecase struct {
	tr repository.UserDataRepository
}

var _ UserDataReposInterface = &UserDataUsecase{}

type UserDataReposInterface interface {
	Save(*entity.UserData) (*entity.UserData, error)
	Update(*entity.UserData) (*entity.UserData, error)
	GetById(uint64) (*entity.UserData, error)
	GetByUserId(uint64) (*entity.UserData, error)
}

func (t *UserDataUsecase) Save(todos *entity.UserData) (*entity.UserData, error) {
	return t.tr.Save(todos)
}

func (t *UserDataUsecase) Update(todos *entity.UserData) (*entity.UserData, error) {
	return t.tr.Update(todos)
}

func (t *UserDataUsecase) GetById(u uint64) (*entity.UserData, error) {
	return t.tr.GetById(u)
}

func (t *UserDataUsecase) GetByUserId(u uint64) (*entity.UserData, error) {
	return t.tr.GetByUserId(u)
}
