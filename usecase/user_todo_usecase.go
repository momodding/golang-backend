package usecase

import (
	"go-todos/domain/entity"
	"go-todos/domain/repository"
)

type UserTodoUsecase struct {
	tr   repository.UserTodoRepository
	user UserReposInterface
}

var _ UserTodoReposInterface = &UserTodoUsecase{}

type UserTodoReposInterface interface {
	Save(*entity.UserTodo) (*entity.UserTodo, error)
	Update(*entity.UserTodo) (*entity.UserTodo, error)
	Delete(uint64) error
	GetById(uint64) (*entity.UserTodo, error)
	GetAll() ([]entity.UserTodo, error)
}

func (t *UserTodoUsecase) Save(todos *entity.UserTodo) (*entity.UserTodo, error) {
	return t.tr.Save(todos)
}

func (t *UserTodoUsecase) Update(todos *entity.UserTodo) (*entity.UserTodo, error) {
	return t.tr.Update(todos)
}

func (t *UserTodoUsecase) Delete(u uint64) error {
	return t.tr.Delete(u)
}

func (t *UserTodoUsecase) GetById(u uint64) (*entity.UserTodo, error) {
	return t.tr.GetById(u)
}

func (t *UserTodoUsecase) GetAll() ([]entity.UserTodo, error) {
	return t.tr.GetAll()
}
