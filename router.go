package main

import (
	"github.com/gofiber/fiber/v2"
	"go-todos/handler"
	"go-todos/handler/middleware"
	"go-todos/infrastructure/auth"
	"go-todos/infrastructure/persistence"
	"go-todos/infrastructure/response"
)

func router(r *fiber.App, services *persistence.Repositories) {
	tk := auth.NewToken()
	resp := response.NewResponseHandler()

	todosHandler := handler.NewTodos(services.UserTodo, services.UserCredential, resp)
	authHandler := handler.NewAuth(services.UserCredential, services.UserToken, tk, resp)

	v1 := r.Group("/v1")

	v1.Post("/auth/login", authHandler.Login)
	v1.Get("/auth/token", authHandler.RefreshToken)
	v1.Post("/auth/register", authHandler.Register)

	v1.Post("/todo", middleware.AuthFilter(), todosHandler.SaveTodo)
	v1.Put("/todo/:todo_id", middleware.AuthFilter(), todosHandler.UpdateTodo)
	v1.Get("/todo/:todo_id", middleware.AuthFilter(), todosHandler.GetTodo)
	v1.Get("/todo/:todo_id/mapped", middleware.AuthFilter(), todosHandler.GetMappedTodo)
	v1.Delete("/todo/:todo_id", middleware.AuthFilter(), todosHandler.DeleteTodo)
	v1.Get("/todo", middleware.AuthFilter(), todosHandler.GetAllTodo)
}
