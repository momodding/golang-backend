package handler

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gofiber/fiber/v2"
	"go-todos/domain/entity"
	"go-todos/infrastructure/auth"
	"go-todos/infrastructure/response"
	"go-todos/usecase"
	"net/http"
	"strconv"
)

type TodoRequest struct {
	TaskName string `json:"name"`
	TaskDesc string `json:"description"`
}

type Todos struct {
	todoApp         usecase.UserTodoReposInterface
	userApp         usecase.UserReposInterface
	responseHandler response.ResponseHandlerInterface
}

func NewTodos(
	todoApp usecase.UserTodoReposInterface,
	userApp usecase.UserReposInterface,
	resp response.ResponseHandlerInterface,
) *Todos {
	return &Todos{
		todoApp:         todoApp,
		userApp:         userApp,
		responseHandler: resp,
	}
}

func (t *Todos) SaveTodo(c *fiber.Ctx) error {
	request := new(TodoRequest)
	if err := c.BodyParser(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}
	if err := request.validateTodoCreation(); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	tokenData, err := auth.GetUserFromToken(c)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	savedTodos, err := t.todoApp.Save(&entity.UserTodo{
		TaskName:  request.TaskName,
		TaskDesc:  request.TaskDesc,
		UpdatedBy: tokenData.UserId,
	})
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	return c.Status(fiber.StatusCreated).JSON(t.responseHandler.ResponseBody(
		true,
		savedTodos,
		"create success"))
}

func (t *Todos) UpdateTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, "invalid request")
	}

	request := new(TodoRequest)
	if err := c.BodyParser(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}
	if err := request.validateTodoCreation(); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	todos, err := t.todoApp.GetById(todoId)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, "todo not found")
	}

	todos.TaskName = request.TaskName
	todos.TaskDesc = request.TaskDesc

	tokenData, err := auth.GetUserFromToken(c)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	todos.UpdatedBy = tokenData.UserId
	updateTodos, err := t.todoApp.Update(todos)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(
		true,
		updateTodos,
		"update success"))
}

func (t *Todos) DeleteTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, "invalid request")
	}

	_, err = t.todoApp.GetById(todoId)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, "todo not found")
	}

	err = t.todoApp.Delete(todoId)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(
		true,
		nil,
		"delete success"))
}

func (t *Todos) GetTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return fiber.NewError(http.StatusBadRequest, "invalid request")
	}

	todos, err := t.todoApp.GetById(todoId)
	if err != nil {
		return fiber.NewError(http.StatusNotFound, "todo not found")
	}

	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(
		true,
		todos,
		"fetch success"))
}

func (t *Todos) GetMappedTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return fiber.NewError(http.StatusBadRequest, "invalid request")
	}

	todos, err := t.todoApp.GetById(todoId)
	if err != nil {
		return fiber.NewError(http.StatusNotFound, "todo not found")
	}

	users, err := t.userApp.GetById(todos.UpdatedBy)
	if err != nil {
		users = &entity.User{Username: "undefined"}
	}

	return c.Status(fiber.StatusOK).JSON(
		t.responseHandler.ResponseBody(
			true,
			fiber.Map{
				"name":        todos.TaskName,
				"description": todos.TaskDesc,
				"creator":     users.Username,
			},
			"fetch success"))
}

func (t *Todos) GetAllTodo(c *fiber.Ctx) error {
	todos, err := t.todoApp.GetAll()
	if err != nil {
		return fiber.NewError(http.StatusNotFound, "todo not found")
	}

	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(
		true,
		todos,
		"fetch success"))
}

func (request TodoRequest) validateTodoCreation() error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.TaskName, validation.Required, validation.NilOrNotEmpty),
		validation.Field(&request.TaskDesc, validation.Required, validation.NilOrNotEmpty),
	)
}
