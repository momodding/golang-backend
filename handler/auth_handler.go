package handler

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gofiber/fiber/v2"
	"go-todos/domain/entity"
	"go-todos/infrastructure/auth"
	"go-todos/infrastructure/response"
	"go-todos/usecase"
	"time"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RegistrationRequest struct {
	Username        string `json:"username"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
	Email           string `json:"email"`
	Role            uint64 `json:"role"`
}

type Auth struct {
	userRepos       usecase.UserReposInterface
	tokenRepos      usecase.UserTokenReposInterface
	auth            auth.TokenInterface
	responseHandler response.ResponseHandlerInterface
}

func NewAuth(
	userRepos usecase.UserReposInterface,
	tokenRepos usecase.UserTokenReposInterface,
	auth auth.TokenInterface,
	resp response.ResponseHandlerInterface,
) *Auth {
	return &Auth{
		userRepos:       userRepos,
		tokenRepos:      tokenRepos,
		auth:            auth,
		responseHandler: resp,
	}
}

func (t *Auth) Login(c *fiber.Ctx) error {
	loginRequest := new(LoginRequest)
	if err := c.BodyParser(loginRequest); err != nil {
		return err
	}
	if err := loginRequest.validateLogin(); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	users, err := t.userRepos.GetByEmailOrUsernameWhereActive(loginRequest.Username, loginRequest.Username)
	if err != nil {
		return fiber.NewError(fiber.StatusUnauthorized, "login gagal")
	}
	if users.Password != loginRequest.Password {
		return fiber.NewError(fiber.StatusUnauthorized, "login gagal")
	}

	token, err := t.auth.CreateToken(users.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	_, _ = t.tokenRepos.Save(&entity.UserToken{
		User:         users.ID,
		RefreshToken: token.RefreshToken,
		ExpireIn:     time.Now().Add(time.Hour * time.Duration(8)),
	})

	c.Cookie(&fiber.Cookie{
		Name:     "accessToken",
		Value:    token.AccessToken,
		Expires:  time.Now().Add(15 * time.Minute),
		SameSite: "lax",
		Secure:   false,
		HTTPOnly: true,
	})
	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(true, fiber.Map{
		"accessToken":  token.AccessToken,
		"email":        users.Email,
		"role":         users.Role,
		"refreshToken": token.RefreshToken,
		"username":     users.Username,
	}, "login sukses"))
}

func (t *Auth) Register(c *fiber.Ctx) error {
	registerRequest := new(RegistrationRequest)
	if err := c.BodyParser(registerRequest); err != nil {
		return err
	}
	if err := registerRequest.validateRegister(); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	users, err := t.userRepos.GetByEmailOrUsernameWhereActive(registerRequest.Email, registerRequest.Username)
	if err == nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, "username or email exist")
	}
	if registerRequest.Password != registerRequest.PasswordConfirm {
		return fiber.NewError(fiber.StatusUnprocessableEntity, "password not match")
	}

	users, err = t.userRepos.Save(&entity.User{
		Username:     registerRequest.Username,
		Password:     registerRequest.Password,
		Email:        registerRequest.Email,
		Role:         registerRequest.Role,
		Status:       "Y",
		LoginAttempt: 0,
	})
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	token, err := t.auth.CreateToken(users.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	_, _ = t.tokenRepos.Save(&entity.UserToken{
		User:         users.ID,
		RefreshToken: token.RefreshToken,
		ExpireIn:     time.Now().Add(time.Hour * time.Duration(8)),
	})

	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(true, fiber.Map{
		"accessToken":  token.AccessToken,
		"email":        users.Email,
		"role":         users.Role,
		"refreshToken": token.RefreshToken,
		"username":     users.Username,
	}, "login sukses"))
}

func (t *Auth) RefreshToken(c *fiber.Ctx) error {
	refreshTokenRequest := c.Query("refreshToken")

	tokenData, err := t.tokenRepos.GetByRefreshToken(refreshTokenRequest)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, err.Error())
	}

	user, err := t.userRepos.GetById(tokenData.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, err.Error())
	}

	token, err := t.auth.CreateToken(user.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	return c.Status(fiber.StatusOK).JSON(t.responseHandler.ResponseBody(true, fiber.Map{
		"accessToken":  token.AccessToken,
		"refreshToken": token.RefreshToken,
		"expireIn":     tokenData.ExpireIn,
	}, "login sukses"))
}

func (request LoginRequest) validateLogin() error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Username, validation.Required),
		validation.Field(&request.Password, validation.Required),
	)
}

func (request RegistrationRequest) validateRegister() error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Username, validation.Required),
		validation.Field(&request.Email, validation.Required),
		validation.Field(&request.Password, validation.Required),
		validation.Field(&request.PasswordConfirm, validation.Required),
		validation.Field(&request.Role, validation.Required),
	)
}
