package persistence

import (
	"errors"
	"github.com/jinzhu/gorm"
	"go-todos/domain/entity"
	"go-todos/domain/repository"
	"time"
)

type UserTokenRepo struct {
	db *gorm.DB
}

func (r *UserTokenRepo) Save(token *entity.UserToken) (*entity.UserToken, error) {
	err := r.db.Debug().Create(&token).Error
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (r *UserTokenRepo) Update(token *entity.UserToken) (*entity.UserToken, error) {
	err := r.db.Debug().Save(&token).Error
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (r *UserTokenRepo) Delete(id uint64) error {
	var token entity.UserToken
	err := r.db.Debug().Where("ut_id = ?", id).Delete(&token).Error
	if err != nil {
		return errors.New("database error, please try again")
	}
	return nil
}

func (r *UserTokenRepo) GetById(id uint64) (*entity.UserToken, error) {
	var token entity.UserToken
	err := r.db.Debug().Where("ut_id = ?", id).Take(&token).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("token not found")
	}
	return &token, nil
}

func (r *UserTokenRepo) GetByRefreshToken(s string) (*entity.UserToken, error) {
	var token entity.UserToken
	err := r.db.Debug().Where("ut_token = ? and ut_expire_in >= ?", s, time.Now()).Take(&token).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("token not expired")
	}
	return &token, nil
}

func (r *UserTokenRepo) GetAll() ([]entity.UserToken, error) {
	var token []entity.UserToken
	err := r.db.Debug().Limit(100).Order("uc_created_at desc").Find(&token).Error
	if err != nil {
		return nil, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("user not found")
	}
	return token, nil
}

func NewUserTokenRepository(db *gorm.DB) *UserTokenRepo {
	return &UserTokenRepo{db}
}

var _ repository.UserTokenRepository = &UserTokenRepo{}
