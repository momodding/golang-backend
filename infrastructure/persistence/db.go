package persistence

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"go-todos/domain/repository"
)

type Repositories struct {
	UserTodo       repository.UserTodoRepository
	UserCredential repository.UserCredentialRepository
	UserToken      repository.UserTokenRepository
	UserData       repository.UserDataRepository
	db             *gorm.DB
}

func NewRepositories(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*Repositories, error) {
	//DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
	db, err := gorm.Open(Dbdriver, DBURL)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)

	return &Repositories{
		UserTodo:       NewUserTodoRepository(db),
		UserCredential: NewUserCredentialRepository(db),
		UserToken:      NewUserTokenRepository(db),
		UserData:       NewUserDataRepository(db),
		db:             db,
	}, nil
}

//closes the  database connection
func (s *Repositories) Close() error {
	return s.db.Close()
}
