package response

import "github.com/gofiber/fiber/v2"

type ResponseHandler struct{}

func NewResponseHandler() *ResponseHandler {
	return &ResponseHandler{}
}

func (r *ResponseHandler) ResponseBody(status bool, data interface{}, messsage string) map[string]interface{} {
	return fiber.Map{
		"status":  status,
		"data":    data,
		"message": messsage,
	}
}

type ResponseHandlerInterface interface {
	ResponseBody(status bool, data interface{}, messsage string) map[string]interface{}
}

//UserToken implements the TokenInterface
var _ ResponseHandlerInterface = &ResponseHandler{}
