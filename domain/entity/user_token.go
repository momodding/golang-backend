package entity

import (
	"html"
	"strings"
	"time"
)

type UserToken struct {
	ID           uint64    `gorm:"column:ut_id;primary_key;auto_increment" json:"id"`
	User         uint64    `gorm:"column:ut_uc_id;int0;not null"`
	RefreshToken string    `gorm:"column:ut_token;varchar:100;not null" json:"refreshToken"`
	ExpireIn     time.Time `gorm:"column:ut_expire_in"`
	CreatedAt    time.Time `gorm:"column:ut_created_at;default:CURRENT_TIMESTAMP"`
}

type UserTokenTable interface {
	TableName() string
}

func (UserToken) TableName() string {
	return "user_token"
}

func (entity *UserToken) BeforeSave() {
	entity.RefreshToken = html.EscapeString(strings.TrimSpace(entity.RefreshToken))
}

func (entity *UserToken) Prepare() {
	entity.RefreshToken = html.EscapeString(strings.TrimSpace(entity.RefreshToken))
	entity.CreatedAt = time.Now()
}
