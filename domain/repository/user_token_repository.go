package repository

import "go-todos/domain/entity"

type UserTokenRepository interface {
	Save(*entity.UserToken) (*entity.UserToken, error)
	Update(*entity.UserToken) (*entity.UserToken, error)
	Delete(uint64) error
	GetById(uint64) (*entity.UserToken, error)
	GetByRefreshToken(string) (*entity.UserToken, error)
	GetAll() ([]entity.UserToken, error)
}
