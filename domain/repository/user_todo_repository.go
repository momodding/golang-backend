package repository

import "go-todos/domain/entity"

type UserTodoRepository interface {
	Save(*entity.UserTodo) (*entity.UserTodo, error)
	Update(*entity.UserTodo) (*entity.UserTodo, error)
	Delete(uint64) error
	GetById(uint64) (*entity.UserTodo, error)
	GetAll() ([]entity.UserTodo, error)
}
