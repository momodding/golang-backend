package repository

import "go-todos/domain/entity"

type UserDataRepository interface {
	Save(*entity.UserData) (*entity.UserData, error)
	Update(*entity.UserData) (*entity.UserData, error)
	GetById(uint64) (*entity.UserData, error)
	GetByUserId(uint64) (*entity.UserData, error)
}
